<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" >
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body>
<!-- start header -->
<div id="wrapper">
<div id="header">

	<div id="logo">
		<h1>
         <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
         <?php print $site_name; ?></a>
        </h1>
		<p><?php print $site_slogan; ?></p>
	</div>
	<div id="search">
	 <?php print $search_box; ?>  
	</div>
</div>
<!-- end header -->
<!-- start menu -->
<div id="menu">
	<?php if (isset($primary_links)) : ?>
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
          <?php endif; ?>
</div>
<!-- end menu -->
<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<h1 class="pagetitle"><?php print $title; ?></h1>
		<a href="<?php print $base_url; ?>/rss.xml" id="rss-posts">RSS Feed</a>
        <div id="contentBody">
  
		<?php print $mission; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content; ?>
        </div>
        </div>
	<div id="sidebar">
		<?php print $left; ?>
	</div>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div><!-- end wrapper -->
<div id="footer">
<?php print $footer_message; ?>
	<p>Design by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a> &nbsp;&bull;&nbsp; Icons by <a href="http://www.famfamfam.com/">FAMFAMFAM</a>. | Themed by <a href="http://anthonylicari.com" title="Web Development, BlackBerry, Drupal, Wordpress Themes and Tutorials">Anthony Licari</a></p>
        <?php print $closure; ?>
</div>
</body>
</html>
